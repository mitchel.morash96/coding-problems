#include <iostream>
#include <vector>
using namespace std;

vector<int> plusOne(vector<int>& digits) {

    if (digits.back() == 9 && digits.size() == 1) {
        digits.back() = 1;
        digits.push_back(0);
    } else if (digits.back() == 9) {
        for (int i = digits.size()-1; i > 0; i--) {
            if (digits[i] == 9) {
                digits[i] = 0;
            } else {
                digits[i] += 1;
                break;
            }
        }
    } else {
        digits.back() += 1; 
    }

    return digits; 
}

int main() {
    vector<int> test1 = {1,2,3};    // == 124
    vector<int> test2 = {9};        // == 10
    vector<int> test3 = {1,7,9};    // == 180
    vector<int> test4 = {1,3,9,9};  // == 1400

    plusOne(test1);
    plusOne(test2);
    plusOne(test3);
    plusOne(test4);

    return 0;
}