#include <iostream>
#include <vector>
using namespace std;

int removeElement(vector<int>& nums, int val) {
    int count = 0;
    for (int i = 0; i < nums.size(); i++) {
        if (nums[i] == val) {
            count++;
            nums[i]=nums[nums.size()-count];
            nums[nums.size()-count] = -1;
            i--;
        }
    }

    //test print
    for (int j = 0; j < nums.size(); j++) {
        cout << nums[j];
    }
    cout << endl;
    //
    return nums.size()-count;
}

int main() {

    vector<int> test1 = {3, 2, 2, 3};
    vector<int> test2 = {0, 1, 2, 2, 3, 0, 4, 2};

    cout << removeElement(test1, 3) << endl;
    cout << removeElement(test2, 2) << endl;

    return 0;
}