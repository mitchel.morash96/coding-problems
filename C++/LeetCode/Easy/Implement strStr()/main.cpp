#include <iostream>
#include <string>
using namespace std;

int strStr(string haystack, string needle) {
    return haystack.find(needle);
}

int main() {

    cout << strStr("hello", "ll") << endl;
    cout << strStr("aaaaa", "bba") << endl;
    cout << strStr("", "") << endl;

    return 0;
}