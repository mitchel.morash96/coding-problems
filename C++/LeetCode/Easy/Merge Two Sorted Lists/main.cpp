#include <iostream>
using namespace std;

// Definition for singly-linked list.
struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

ListNode* mergeTwoLists(ListNode* l1, ListNode* l2) {
    if (l1 == NULL && l2 == NULL)
        return NULL;

    if (l1 == NULL) {
        return l2;
    } else if (l2 == NULL) {
        return l1;
    }

    bool isTrue = true;
    int count = 0;

    ListNode *tmp;
    tmp = l1;
    while (isTrue) {
        if (tmp->next == NULL) {
            isTrue = false;
        } else {
            tmp = tmp->next;
        }
    }
    tmp->next = l2;

    // Get Count
    ListNode *tmp2;
    tmp2 = l1;
    while (tmp2 != NULL) {
        count++;
        tmp2 = tmp2->next;
    }

    ListNode *h;
    bool swapped = false;
    for (int i = 0; i < count; i++) {
        h = l1;
        swapped = false;

        for (int j = 0; j < count-i-1; j++) {
            ListNode *p1 = h;
            ListNode *p2 = p1->next;

            if (p1->val > p2->val) {
                int num = p2->val;
                p2->val = p1->val;
                p1->val = num;

                swapped = true;
            }
            h = h->next;
        }

        if (!swapped) {
            break;
        }
    }

    // Print
    ListNode *tmp3;
    tmp3 = l1;
    while (tmp3 != NULL) {
        cout << tmp3->val << endl;
        tmp3 = tmp3->next;
    }

    return l1;
} 

int main() {
    // test 1
    ListNode *n2 = new ListNode(4);
    ListNode *n1 = new ListNode(2, n2);
    ListNode *n = new ListNode(1, n1);
    ListNode *m2 = new ListNode(4);
    ListNode *m1 = new ListNode(3, m2);
    ListNode *m = new ListNode(1, m1);

    // test 3
    ListNode *b = new ListNode(0);

    mergeTwoLists(n, m);
    mergeTwoLists(NULL, NULL);
    mergeTwoLists(NULL, b);

    return 0;
}