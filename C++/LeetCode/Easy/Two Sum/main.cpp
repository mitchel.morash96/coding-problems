#include <iostream>
#include <vector>
using namespace std;

vector<int> twoSum(vector<int>& nums, int target) {
    vector<int> indexs;

    for (int i = 0; i < nums.size(); i++) {
        for (int j = 0; j <nums.size(); j++) {
            if (i != j) {
                if (nums[i]+nums[j] == target) {
                    indexs.push_back(i);
                    indexs.push_back(j);
                    return indexs;
                }
            }
        }
    }
}

int main() {

    vector<int> test1 = {2, 7, 11, 15};
    vector<int> test2 = {3, 2, 4};
    vector<int> test3 = {3, 3};

    twoSum(test1, 9);
    twoSum(test2, 6);
    twoSum(test3, 6);

    return 0;
}