#include <iostream>
#include <string>

bool isPalindrome(int x) {
    std::string str = std::to_string(x);
    for (int i = 0; i < str.size(); i++) {
        if (str[i] != str[str.size()-1-i]) {
            return false;
        }
    }
    return true;
}

int main() {

    std::cout << isPalindrome(121) << std::endl;
    std::cout << isPalindrome(-121) << std::endl;
    std::cout << isPalindrome(10) << std::endl;
    std::cout << isPalindrome(-101) << std::endl;

    return 0;
}