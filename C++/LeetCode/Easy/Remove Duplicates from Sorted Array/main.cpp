#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int removeDuplicates(vector<int>& nums) {

    nums.erase (unique(nums.begin(), nums.end()), nums.end());

    return nums.size();
}

int main() {

    vector<int> test1 = {1,1,2};
    vector<int> test2 = {0,0,1,1,1,2,2,3,3,4};

    cout << removeDuplicates(test1) << endl;
    cout << removeDuplicates(test2) << endl;

    return 0;
}