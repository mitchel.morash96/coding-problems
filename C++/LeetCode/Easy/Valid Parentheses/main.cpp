#include <iostream>
#include <string>
#include <stack>
using namespace std;

bool isValid(string s) {
    stack<char> stack;

    for (int i = 0; i < s.size(); i++) {
        if (s[i] == '(' || s[i] == '[' || s[i] == '{') {
            stack.push(s[i]);
        } else {
            if (stack.size() == 0) {
                return false;
            }

            char top = stack.top();
            stack.pop();
            
            if (s[i]==')' && (top == '{' || top == '[')) {
                return false;
            } else if (s[i]=='}' && (top == '(' || top == '[')) {
                return false;
            } else if (s[i]==']' && (top == '(' || top == '{')) { 
                return false;
            }
        }
    }

    if (stack.size() == 0) {
        return true;
    } else {
        return false;
    }
}

int main() {

    cout << isValid("()") << endl;
    cout << isValid("()[]{}") << endl;
    cout << isValid("(]") << endl;
    cout << isValid("([)]") << endl;
    cout << isValid("{[]}") << endl;

    return 0;
}