#include <iostream>
#include <string>
#include <map>
using namespace std;

int romanToInt(string s) {
    map<char, int> m = {{'I', 1},{'V', 5},{'X', 10},{'L', 50},{'C', 100},{'D', 500},{'M', 1000}};
    int res = 0;

    for (int i = 0; i < s.size(); i++) {

        if (s[i] == 'I') {
            if (s[i+1] == 'V') {
                res += 4;
                i++;
            } else if (s[i+1] == 'X') {
                res += 9;
                i++;
            } else {
                res += m.at(s[i]);
            }
        } else if (s[i] == 'X') {
            if (s[i+1] == 'L') {
                res += 40;
                i++;
            } else if (s[i+1] == 'C') {
                res += 90;
                i++;
            } else {
                res += m.at(s[i]);
            }
        } else if (s[i] == 'C') {
            if (s[i+1] == 'D') {
                res += 400;
                i++;
            } else if (s[i+1] == 'M') {
                res += 900;
                i++;
            } else {
                res += m.at(s[i]);
            }
        } else {
            res += m.at(s[i]);
        }    

    }

    return res;
}

int main() {

    cout << romanToInt("III") << endl;
    cout << romanToInt("IV") << endl;
    cout << romanToInt("IX") << endl;
    cout << romanToInt("LVIII") << endl;
    cout << romanToInt("MCMXCIV") << endl;

    return 0;
}