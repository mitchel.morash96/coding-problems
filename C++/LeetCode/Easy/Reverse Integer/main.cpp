#include <iostream>
#include <string>
#include <algorithm>
using namespace std;

int reverse(int x) {
    string str = to_string(x);

    try {
        if (x < 0) {
            reverse(str.begin(), str.end());
            return -stoi(str);
        } else {
            reverse(str.begin(), str.end());
            return stoi(str);
        }
    } catch (::out_of_range) {
        return 0;
    }

    return 0;
}

int main() {

    cout << reverse(123) << endl;
    cout << reverse(-123) << endl;
    cout << reverse(120) << endl;
    cout << reverse(0) << endl;
    cout << reverse(1534236469) << endl;

    return 0;
}