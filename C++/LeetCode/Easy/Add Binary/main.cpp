#include <iostream>
#include <string>
using namespace std;

string addBinary(string a, string b) {
    string result = "";
    int i = a.size()-1;
    int j = b.size()-1;
    int sum = 0;

    while(i >= 0 || j >= 0 || sum == 1) {
        
        sum += ((i >= 0)? a[i]-'0':0);
        sum += ((j >= 0)? b[j]-'0':0);

        result = char(sum%2 + '0') + result;

        sum /= 2;

        i--; j--;
    }
    return result;
}

int main() {

    cout << addBinary("11", "1") << endl;
    cout << addBinary("1010", "1011") << endl;

    return 0;
}