#include <iostream>
#include <vector>
using namespace std;

int searchInsert(vector<int>& nums, int target) {

    for (int i = 0; i < nums.size(); i++) {
        if (nums[i] >= target) {
            return i;
        }
    }

    return nums.size();
}

int main() {

    vector<int> test1 = {1,3,5,6};
    vector<int> test2 = {1};

    cout << searchInsert(test1, 5) << endl;
    cout << searchInsert(test1, 2) << endl;
    cout << searchInsert(test1, 7) << endl;
    cout << searchInsert(test1, 0) << endl;
    cout << searchInsert(test2, 0) << endl;

    return 0;
}