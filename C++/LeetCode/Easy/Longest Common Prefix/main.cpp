#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

string longestCommonPrefix(vector<string>& strs) {
    int len = strs[0].size();

    for (int i = 0; i < strs.size(); i++) {      
        if (strs[i].size() < len) {
            len = strs[i].size();
        }
        for (int j = 0; j < len; j++) {
            if (strs[0][j] != strs[i][j]) {
                len = j;
                break;
            }
        }
    }

    return strs[0].substr(0, len);
}

int main() {

    vector<string> test1 = {"flower", "flow", "flight"};
    vector<string> test2 = {"dog", "racecar", "car"};

    cout << longestCommonPrefix(test1) << endl;
    cout << longestCommonPrefix(test2) << endl;

    return 0;
}