#include <iostream>

int divide(int dividend, int divisor) {
    
    if (divisor == 0 || dividend == INT32_MIN && divisor == -1) {
        return INT32_MAX;
    }

    return dividend/divisor; 
    
}

int main() {

    //std::cout << divide(10, 3) << std::endl;
    //std::cout << divide(7, -3) << std::endl;
    //std::cout << divide(0, 1) << std::endl;
    //std::cout << divide(1, 1) << std::endl;
    std::cout << divide(-2147483648, -1) << std::endl;

    return 0;
}