#include <iostream>
#include <string>
#include <vector>
#include <map>
using namespace std;

void backtrack(vector<string>& combinations, map<char, string>& m, string input, string outPut, int index) {
    if (index == input.size()) {
        combinations.push_back(outPut);
        return;
    }

    for (int i = 0; i < m[input[index]].size(); i++) {
        outPut.push_back(m[input[index]][i]);
        backtrack(combinations, m, input, outPut, index+1);
        outPut.pop_back();
    }
}

vector<string> letterCombinations(string digits) {    
    if (digits.size() == 0) {
        return {};
    } 
    map<char, string> m = {{'2', "abc"}, {'3', "def"}, {'4', "ghi"}, {'5', "jkl"}, 
                          {'6', "mno"}, {'7', "pqrs"}, {'8', "tuv"}, {'9', "wxyz"}};
    vector<string> combs;  
    string outPut;
    
    backtrack(combs, m, digits, outPut, 0);

    return combs;
}

int main() {

    vector<string> test;

    //letterCombinations("");
    //letterCombinations("2");
    //letterCombinations("23");
    test = letterCombinations("234");
    //letterCombinations("2345");

    for (int i = 0; i < test.size(); i++) {
        cout << test[i] << endl;
    }

    return 0;
}