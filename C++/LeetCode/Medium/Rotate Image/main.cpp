#include <iostream>
#include <vector>
using namespace std;

void rotate(vector<vector<int>>& matrix) {
    int top = 0;
    int bottom = matrix.size()-1;
    int left = 0;
    int right = matrix.size()-1;;

    while (left < right) {
        for (int i = 0; i < right-left; i++) {
            int tmp = matrix[top][left+i];
            matrix[top][left+i] = matrix[bottom-i][left];
            matrix[bottom-i][left] = matrix[bottom][right-i];
            matrix[bottom][right-i] = matrix[top+i][right];
            matrix[top+i][right] = tmp;
        }
        top++;
        left++;
        bottom--;
        right--;
    }

    // test print   
    for (int m = 0; m < matrix.size(); m++) {
        for (int n = 0; n < matrix[0].size(); n++) {
            cout << matrix[m][n] << " ";
        }
        cout << endl;
    }
    // test print
}

int main() {

    //vector<vector<int>> test1 = {{1,2,3},{4,5,6},{7,8,9}};
    //vector<vector<int>> test2 = {{5,1,9,11},{2,4,8,10},{13,3,6,7},{15,14,12,16}};
    //vector<vector<int>> test3 = {{1}};
    vector<vector<int>> test4 = {{1,2},{3,4}};

    //rotate(test1);
    //rotate(test2);
    //rotate(test3);
    rotate(test4);

    return 0;
}