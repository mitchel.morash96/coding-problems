#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

vector<vector<string>> groupAnagrams(vector<string>& strs) {
    vector<string> copy;
    vector<string> uniqueStrs = strs;
    vector<vector<string>> out;
    
    for (int i = 0; i < uniqueStrs.size(); i++) {
        sort(uniqueStrs[i].begin(), uniqueStrs[i].end());
    }

    copy = uniqueStrs;
    sort(uniqueStrs.begin(), uniqueStrs.end());
    uniqueStrs.erase( unique( uniqueStrs.begin(), uniqueStrs.end()), uniqueStrs.end());

    for (int j = 0; j < uniqueStrs.size(); j++) {
        vector<string> temp = {};
        for (int k = 0; k < strs.size(); k++) {
            if (uniqueStrs[j] == copy[k]) {
                temp.push_back(strs[k]);
            }
        }
        out.push_back(temp);
    }

    return out;
}

int main() {

    vector<string> test1 = {"eat", "tea", "tan", "ate", "nat", "bat"};
    //vector<string> test2 = {""};
    //vector<string> test3 = {"a"};

    groupAnagrams(test1);
    //groupAnagrams(test2);
    //groupAnagrams(test3);

    return 0;
}