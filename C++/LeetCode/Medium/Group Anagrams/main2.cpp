#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <map>
using namespace std;

vector<vector<string>> groupAnagrams(vector<string>& strs) {
    vector<vector<string>> output;
    map<string, vector<string>> m;
    string temp;

    if (strs.size() == 0) {
        return {};
    }

    for (auto word : strs) {
        temp = word;
        sort(word.begin(), word.end());
        m[word].push_back(temp);
    }

    for (auto i : m) {
        output.push_back(i.second);
    }

    return output;
}

int main() {

    vector<string> test1 = {"eat", "tea", "tan", "ate", "nat", "bat"};
    //vector<string> test2 = {""};
    //vector<string> test3 = {"a"};

    groupAnagrams(test1);
    //groupAnagrams(test2);
    //groupAnagrams(test3);

    return 0;
}