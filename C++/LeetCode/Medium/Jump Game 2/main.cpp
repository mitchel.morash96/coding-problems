#include <iostream>
#include <vector>
using namespace std;

int jump(vector<int>& nums) {
    int min_r = 0; 
    int max_r = 0;
    int steps = 0;
    int i = min_r;

    while(max_r < nums.size()-1) {
        steps++;
        min_r = max_r;
        for (; i <= min_r; i++) {
            max_r = max(max_r, i + nums[i]);
        }
    }
    return steps;
}

int main() {

    vector<int> test1 = {2, 3, 1, 1, 4};
    vector<int> test2 = {2, 3, 0, 1, 4};
    vector<int> test3 = {5,6,4,4,6,9,4,4,7,4,4,8,2,6,8,1,5,9,6,5,2,7,9,7,9,6,9,4,1,6,8,8,4,4,2,0,3,8,5};

    cout << jump(test1) << endl;
    cout << jump(test2) << endl;
    cout << jump(test3) << endl;

    return 0;
}