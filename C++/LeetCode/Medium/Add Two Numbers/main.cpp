#include <iostream>
using namespace std;

// Definition for singly-linked list.
struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
    ListNode *result = new ListNode(0);
    ListNode *cur = result;
    int carry = 0, curNum;

    while (l1 != NULL || l2 != NULL) {
        int num1 = 0, num2 = 0;

        if (l1 != NULL) {
            num1 = l1->val;
            l1 = l1->next;
        }
        if (l2 != NULL) {
            num2 = l2->val;
            l2 = l2->next;
        }

        curNum = num1 + num2 + carry;
        carry = curNum/10;

        ListNode *node = new ListNode(curNum%10);
        cur->next = node;
        cur = cur->next;
    }
    if (carry) {
        ListNode *node = new ListNode(carry%10);
        carry /= 10;
        cur->next = node;
        cur = cur->next;
    }

    return result->next;
}

int main() {
    // Test1
    ListNode *n2 = new ListNode(3);
    ListNode *n1 = new ListNode(4, n2);
    ListNode *n = new ListNode(2, n1);

    ListNode *m2 = new ListNode(4);
    ListNode *m1 = new ListNode(6, m2);
    ListNode *m = new ListNode(5, m1);

    // Test2
    // ListNode *b = new ListNode(0);

    // ListNode *v = new ListNode(0);

    // Test3
    // ListNode *j6 = new ListNode(9);
    // ListNode *j5 = new ListNode(9, j6);
    // ListNode *j4 = new ListNode(9, j5);
    // ListNode *j3 = new ListNode(9, j4);
    // ListNode *j2 = new ListNode(9, j3);
    // ListNode *j1 = new ListNode(9, j2);
    // ListNode *j = new ListNode(9, j1);

    // ListNode *k3 = new ListNode(9);
    // ListNode *k2 = new ListNode(9, k3);
    // ListNode *k1 = new ListNode(9, k2);
    // ListNode *k = new ListNode(9, k1);

    addTwoNumbers(n, m);
    //addTwoNumbers(b, v);
    //addTwoNumbers(j, k);

    return 0;
}