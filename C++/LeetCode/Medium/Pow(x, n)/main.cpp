#include <iostream>
using namespace std;

double myPow(double x, int n) {
    if (n == 0) {
        return 1;
    }

    int temp = abs(n);
    double res = myPow(x*x, temp/2) * (temp%2 ? x:1);

    if (n > 0) {
        return res;
    } else {
        return 1/res;
    }
}

int main() {

    cout << myPow(2.00000, 10) << endl;
    cout << myPow(2.10000, 3) << endl;
    cout << myPow(2.00000, -2) << endl;

    return 0;
}