#include <iostream>
#include <string>
using namespace std;

bool isMatch(string s, string p) {

    if (s == p) { 
        cout << "PASS(strings match)" << endl;
        return true; 
    }
    if (p == "*") { 
        cout << "PASS(* only char)" << endl;
        return true; 
    }

    int offset = 0;
    for (int i = 0; i+offset < s.size(); i++) {
        if (s[i+offset] != p[i]) {
            if (p[i] == '?') {
                cout << s[i+offset] << " == " << p[i] << " : PASS(?)" << endl;
            } else if (p[i] == '*') {
                string tmp = "";
                for (int j = i; j < s.size(); j++) {
                    if (s[i+j+offset] == p[i+1] && i+j < s.size()) {
                        offset++;

                        cout << tmp << " == " << p[i] << " : PASS(*)" << endl;
                    }
                    tmp.push_back(s[i+j+offset]);
                }
            } else {
                cout << s[i+offset] << " != " << p[i] << " : FAIL" << endl;
                return false;
            }

        } else {
            cout << s[i+offset] << " == " << p[i] << " : PASS" << endl;
        }
    }

    return true;
}

int main() {

    //cout << isMatch("aa", "a") << endl;
    //cout << isMatch("aa", "*") << endl;
    //cout << isMatch("cb", "?a") << endl;
    cout << isMatch("adceb", "*a*b") << endl;
    //cout << isMatch("acdcb", "a*c?b") << endl;

    //cout << isMatch("abcde", "*c?e") << endl;
    //cout << isMatch("abcde", "abcd?") << endl;

    return 0;
}