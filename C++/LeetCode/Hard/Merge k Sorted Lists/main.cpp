#include <iostream>
#include <vector>
using namespace std;

//Definition for singly-linked list.
struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

void combineTwoLists(ListNode* left_list, ListNode* right_list) {
    bool isTrue = true;

    ListNode *tmp;
    tmp = left_list;
    while (isTrue) {
        if (tmp->next == NULL) {
            isTrue = false;
        } else {
            tmp = tmp->next;
        }
    }
    tmp->next = right_list;
}

ListNode* mergeKLists(vector<ListNode*>& lists) {
    int count = 0;

    // Check for && remove empty lists
    for (int k = 0; k < lists.size(); k++) {
        if (lists[k]==NULL) {
            lists.erase(lists.begin() + k);
            k--;
        }
    }
    if (lists.size() == 0) {return NULL;}
    if (lists.size() == 1) {return lists[0];}

    // Combine Lists
    for (int i = 1; i < lists.size(); i++) {
        combineTwoLists(lists[0], lists[i]);
    }

    // Get Count
    ListNode *tmp;
    tmp = lists[0];
    while (tmp != NULL) {
        count++;
        tmp = tmp->next;
    }

    // Sort List (Bubble Sort)
    ListNode *h;
    bool swapped = false;

    for (int i = 0; i < count; i++) {
        h = lists[0];
        swapped = false;

        for (int j = 0; j < count-i-1; j++) {
            ListNode *p1 = h;
            ListNode *p2 = p1->next;

            if (p1->val > p2->val) {
                int num = p2->val;
                p2->val = p1->val;
                p1->val = num;

                swapped = true;
            }
            h = h->next;
        }

        if (!swapped) {
            break;
        }
    }

    // Print
    ListNode *tmp2;
    tmp2 = lists[0];
    while (tmp2 != NULL) {
        cout << tmp2->val << endl;
        tmp2 = tmp2->next;
    }

    return lists[0];
}

int main() {

    ListNode *n2 = new ListNode(5);
    ListNode *n1 = new ListNode(4, n2);
    ListNode *n = new ListNode(1, n1);
    
    ListNode *m2 =  new ListNode(4);
    ListNode *m1 =  new ListNode(3, m2);
    ListNode *m =  new ListNode(1, m1);

    ListNode *b1 =  new ListNode(6);
    ListNode *b =  new ListNode(2, b1);

    //vector<ListNode*> test1 = {n, m, b};
    //vector<ListNode*> test2;
    //vector<ListNode*> test3 = {{}};
    vector<ListNode*> test4 = {{},{}};

    //mergeKLists(test1);
    //mergeKLists(test2);
    //mergeKLists(test3);
    mergeKLists(test4);

    return 0;
}