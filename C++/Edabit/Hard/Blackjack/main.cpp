#include <iostream>
#include <vector>
#include <ctype.h>

bool overTwentyOne(std::vector<char> cards) {
    int count = 0;
    bool ace = false;

    for (int i = 0; i < cards.size(); i++) {
        if (isdigit(cards[i])) {
            count += (int)cards[i] - 48;
        } else if (cards[i] == 'A') {
            ace = true;
            count += 11;
        } else {
            count += 10;
        }
    }

    if (ace) {
        if (count > 21) {
            count -= 10;
        }
    }

    if (count > 21) {
        return true;
    } else {
        return false;
    }
}

int main() {

    std::cout << overTwentyOne({'2', '8', 'J'}) << std::endl;
    std::cout << overTwentyOne({'A', 'J', 'K'}) << std::endl;
    std::cout << overTwentyOne({'5', '5', '3', '9'}) << std::endl;
    std::cout << overTwentyOne({'2', '6', '4', '4', '5'}) << std::endl;
    std::cout << overTwentyOne({'J', 'Q', 'K'}) << std::endl;

}