#include <iostream>
#include <vector>
#include <string>

std::vector<std::string> makeBox(int n) {
    std::vector<std::string> box;

    for (int i = 0; i < n; i++) {
        std::string line = "";
        for (int j = 0; j < n; j++) {
            if (i == 0 || i == n-1) {
                line.push_back('#');
            } else {
                if (j == 0 || j == n-1) {
                    line.push_back('#');
                } else {
                    line.push_back(' ');
                }
            }
        }
        box.push_back(line);
    }

    // print
    for (int k = 0; k < n; k++) {
        std::cout << box[k] << std::endl;
    }

    return box;
}

int main() {

    makeBox(5);
    makeBox(3);
    makeBox(2);
    makeBox(1);

    return 0;
}