#include <iostream>
#include <vector>
#include <map>
using namespace std;

int findOdd(vector<int> arr) {
    map<int, int> countMap;

    for (auto & item : arr) {
        auto result = countMap.insert(pair<int, int>(item, 1));
        if (result.second == false)
            result.first->second++;
    }

    for (auto & item : countMap) {
        if (item.second % 2 != 0) {
            return item.first;
        }
    }
    return 0;
}
 
int main() {

    cout << findOdd({1, 1, 2, -2, 5, 2, 4, 4, -1, -2, 5}) << endl;
    cout << findOdd({20, 1, 1, 2, 2, 3, 3, 5, 5, 4, 20, 4, 5}) << endl;
    cout << findOdd({10}) << endl;

    return 0;
}