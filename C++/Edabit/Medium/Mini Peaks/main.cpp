#include <iostream>
#include <vector>
using namespace std;

vector<int> miniPeaks(vector<int> arr) {
    vector<int> peaks;

    for (int i = 1; i < arr.size()-1; i++) {
        if (arr[i-1] < arr[i] && arr[i] > arr[i+1]) {
            peaks.push_back(arr[i]);
        }
    }

    return peaks;
}

int main() {

    cout << miniPeaks({4, 5, 2, 1, 4, 9, 7, 2}) << endl;
    cout << miniPeaks({1, 2, 1, 1, 3, 2, 5, 4, 4}) << endl;
    cout << miniPeaks({1, 2, 3, 4, 5, 6}) << endl;

    return 0;
}