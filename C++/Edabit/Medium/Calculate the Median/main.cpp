#include <iostream>
#include <vector>
#include <algorithm>

float median(std::vector<int> arr) {
    
    std::sort(arr.begin(), arr.end());
    if (arr.size()%2 != 0) {
        return arr.at(arr.size()/2);
    } else {
        return (arr.at(arr.size()/2) + arr.at(arr.size()/2-1)) / 2.0;
    }
}

int main() {

    std::cout << median({2, 5, 6, 2, 6, 3, 4}) << std::endl;
    //std::cout << median({21.4323, 432.54, 432.3, 542.4567}) << std::endl;
    std::cout << median({-23, -43, -29, -53, -67}) << std::endl;

    return 0;
}