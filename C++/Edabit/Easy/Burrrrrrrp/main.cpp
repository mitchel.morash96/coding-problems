#include <iostream>
#include <string>

using namespace std;

string longBurp(int num) {
    string word = "Bu";

    for (int i = 0; i < num; i++) {
        word += "r";
    }
    word += "p";

    return word;
}

int main() {
    
    cout << longBurp(3) << endl;
    cout << longBurp(5) << endl;
    cout << longBurp(9) << endl;

    return 0;
}

