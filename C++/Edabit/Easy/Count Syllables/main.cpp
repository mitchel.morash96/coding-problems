#include <iostream>
#include <string>

int numberSyllables(std::string word) {
    int count = 1;
    char dash = '-';

    for (int i = 0; i < word.length(); i++) {
        if (word[i] == dash) {
            count++;
        }
    }

    return count;
}

int main() {

    std::cout << numberSyllables("buf-fet") << std::endl;
    std::cout << numberSyllables("beau-ti-ful") << std::endl;
    std::cout << numberSyllables("mon-u-men-tal") << std::endl;
    std::cout << numberSyllables("on-o-mat-o-poe-ia") << std::endl;

    return 0;
}