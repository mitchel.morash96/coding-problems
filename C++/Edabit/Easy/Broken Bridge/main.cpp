#include <iostream>
#include <string>

bool isSafeBridge(std::string s) {
    if (s.find(" ") != std::string::npos) {
        return false;
    } else {
        return true;
    }  
}   

int main() {

    std::cout << isSafeBridge("####") << std::endl;
    std::cout << isSafeBridge("## ####") << std::endl;
    std::cout << isSafeBridge("#") << std::endl;

    return 0;
}