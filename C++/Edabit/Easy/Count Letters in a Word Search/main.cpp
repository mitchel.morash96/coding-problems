#include <iostream>
#include <vector>
using namespace std;

int letterCounter(vector<vector<char>> arr, char c) {
    int count = 0;

    for (int i = 0; i < arr.size(); i++){
        for (int j = 0; j < arr[i].size(); j++) {
            if (arr[i][j] == c) {
                count++;
            }
        }
    }

    return count;
}

int main() {

    cout << letterCounter({{'D', 'E', 'Y', 'H', 'A', 'D'},
                           {'C', 'B', 'Z', 'Y', 'J', 'K'},
                           {'D', 'B', 'C', 'A', 'M', 'N'},
                           {'F', 'G', 'G', 'R', 'S', 'R'},
                           {'V', 'X', 'H', 'A', 'S', 'S'}}, 'D') << endl;

    cout << letterCounter({{'D', 'E', 'Y', 'H', 'A', 'D'},
                           {'C', 'B', 'Z', 'Y', 'J', 'K'},
                           {'D', 'B', 'C', 'A', 'M', 'N'},
                           {'F', 'G', 'G', 'R', 'S', 'R'},
                           {'V', 'X', 'H', 'A', 'S', 'S'}}, 'H') << endl;

    return 0;
}