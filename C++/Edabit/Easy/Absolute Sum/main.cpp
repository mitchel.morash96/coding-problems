#include <iostream>
#include <vector>

int getAbsSum(std::vector<int> arr) {
    int sum = 0;

    for (int i = 0; i < arr.size(); i++) {
        if (arr[i] < 0) {
            sum += -(arr[i]);
        } else {
            sum += arr[i];
        }
    }

    return sum;
}

int main() {

    std::cout << getAbsSum({2, -1, 4, 8, 10}) << std::endl;
    std::cout << getAbsSum({-3, -4, -10, -2, -3}) << std::endl;
    std::cout << getAbsSum({2, 4, 6, 8, 10}) << std::endl;
    std::cout << getAbsSum({-1}) << std::endl;

    return 0;
}