#include <iostream>

int squareAreasDifference(int r) {
    return 2 * r * r;
}

int main() {

    std::cout << squareAreasDifference(5) << std::endl;
    std::cout << squareAreasDifference(6) << std::endl;
    std::cout << squareAreasDifference(7) << std::endl;

    return 0;
}