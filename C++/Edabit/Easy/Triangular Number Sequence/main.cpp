#include <iostream>

int triangle(int num) {

    int sum = 0;

    for (int i = 0; i < num; i++) {
        sum += i+1;
    }

    return sum;
}

int main() {

    std::cout << triangle(1) << std::endl;
    std::cout << triangle(6) << std::endl;
    std::cout << triangle(215) << std::endl;

    return 0;
}