https://edabit.com/challenge/KNxEs8se6jzyHDBfy

Create a function that takes a string and returns a string with its letters 
in alphabetical order.

Examples
alphabetSoup("hello") ➞ "ehllo"

alphabetSoup("edabit") ➞ "abdeit"

alphabetSoup("hacker") ➞ "acehkr"

alphabetSoup("geek") ➞ "eegk"

alphabetSoup("javascript") ➞ "aacijprstv"

Notes
You can assume numbers and punctuation symbols won't be included in test cases. 
You'll only have to deal with single word, alphabetic characters.