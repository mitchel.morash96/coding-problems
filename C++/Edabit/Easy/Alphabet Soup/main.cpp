#include <iostream>
#include <string>
//#include <algorithm>

std::string alphabetSoup(std::string str) {

    //std::sort(str.begin(), str.end());
    int i, j;
    for (i = 0; i < str.length()-1; i++) {
        for (j = 0; j < str.length()-i-1; j++) {
            if (str[j] > str[j+1]) {
                char temp = str[j+1];
                str[j+1] = str[j];
                str[j] = temp;
            }
        }
    }
    return str;
}

int main() {

    std::cout << alphabetSoup("hello") << std::endl;
    std::cout << alphabetSoup("edabit") << std::endl;
    std::cout << alphabetSoup("hacker") << std::endl;
    std::cout << alphabetSoup("geek") << std::endl;
    std::cout << alphabetSoup("javascript") << std::endl; 

    return 0;
}