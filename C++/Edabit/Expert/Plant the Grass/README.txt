https://edabit.com/challenge/szij3YFmix7bMN7En

You will be given a matrix representing a field g and two numbers x, y coordinate.

There are three types of possible characters in the matrix:

x representing a rock.
. representing a blank space.
+ representing a grassed space.

You have to simulate grass growing from the position (x, y). Grass can grow in 
all four directions (up, left, right, down). Grass can only grow on blank spaces 
and can't go past rocks.

Return the simulated matrix.

Examples
simulateGrass({
  "xxxxxxx",
  "x.....x",
  "xxxx..x", 
  "x...xxx", 
  "xxxxxxx"
}, 1, 1) ➞ {
  "xxxxxxx",
  "x+++++x",
  "xxxx++x", 
  "x...xxx", 
  "xxxxxxx"
}

Notes

"xxxxxx",
"xxxx.x",
"xx...x",
"x...xx",
"x..xxx",
"x..xxx",
"xx...x",
"xxx.xx",
"xxxxxx"}, 4, 1) ->

"xxxxxx",
"xxxx+x",
"xx+++x",
"x+++xx",
"x++xxx",
"x++xxx",
"xx+++x",
"xxx+xx",
"xxxxxx"
	

[ xxxxxx, 
  xxxx+x, 
  xx+++x, 
  x+++xx, 
  x++xxx, 
  x++xxx, 
  xx+++x, 
  xxx.xx, 
  xxxxxx ]  