#include <iostream>
#include <vector>
#include <string>

using namespace std;

vector<string> CheckNeigbors(vector<string> g, int x, int y) {
    
    if (y-1 >= 0) {
        if (g[x][y-1] == '.') {
            g[x][y-1] = '+';
            g = CheckNeigbors(g, x, y-1);
        }
    }

    if (y+1 <= g.size()) {
        if (g[x][y+1] == '.') {
            g[x][y+1] = '+';
            g = CheckNeigbors(g, x, y+1);
        }
    }
    
    if (x-1 >= 0) {
        if (g[x-1][y] == '.') {
            g[x-1][y] = '+';
            g = CheckNeigbors(g, x-1, y);
        }
    }
    
    if (x+1 <= g.size()) {
        if (g[x+1][y] == '.') {
            g[x+1][y] = '+';
            g = CheckNeigbors(g, x+1, y);
        }
    }

    return g;
}

vector<string> simulateGrass(vector<string> g, int x, int y) {
    
    vector<string> newGrass(g);
    newGrass[x][y] = '+';
    newGrass = CheckNeigbors(newGrass, x, y);    

    return newGrass;
}

int main() {
    
    simulateGrass({"xxxxxxx", 
                   "x.....x", 
                   "xxxx..x", 
                   "x...xxx", 
                   "xxxxxxx"}, 1, 1);
    
    simulateGrass({"xxxxxx", 
                   "xxxx.x", 
                   "xx...x", 
                   "x...xx", 
                   "x..xxx",
                   "x..xxx",
                   "xx...x",
                   "xxx.xx",
                   "xxxxxx"}, 4, 1);

    return 0;
}