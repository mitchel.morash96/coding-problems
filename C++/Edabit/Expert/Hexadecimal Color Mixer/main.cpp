#include <iostream>
#include <string>
#include <vector>
#include <math.h>
#include <sstream>
#include <algorithm>

std::string hexColorMixer(std::vector<std::string> colors) {
    std::string newColor = "#";
    std::vector<int> values;

    for (int i = 1; i < 7; i+=2) {
        int total = 0;
        for (int k = 0; k < colors.size(); k++) {
            std::string val = colors[k].substr(i, 2);
            total += (std::stoi(val, nullptr, 16));
        }
        values.push_back(round((double)total/colors.size()));
    }
    
    for (int j = 0; j < values.size(); j++) {
        std::stringstream ss;
        ss << std::hex << values[j];
        if (ss.str() == "0") {
            newColor += "00";
        } else {
            newColor += ss.str();
        }  
    }
    
    std::transform(newColor.begin(), newColor.end(), newColor.begin(), ::toupper);

    return newColor;
}

int main() {

    std::cout << hexColorMixer({"#FFFF00", "#FF0000"}) << std::endl;
    std::cout << hexColorMixer({"#FFFF00"}) << std::endl;
    std::cout << hexColorMixer({"#FFFF00", "#0000FF"}) << std::endl;
    std::cout << hexColorMixer({"#B60E73", "#0EAEB6"}) << std::endl;
    std::cout << hexColorMixer({"#FFFFFF", "#000000", "#000000", "#FFFFFF"}) << std::endl;

    return 0;
}