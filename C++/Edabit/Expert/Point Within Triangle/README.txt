https://edabit.com/challenge/kCapSLB9tWKjCQGNE

Create a function that takes four pairs. The first three are (x, y) coordinates 
of three corners of a triangle. Return true if the fourth tuple — 
the (x, y) coordinates of a test point — lies within the triangle, false if 
it doesn't.

Examples
withinTriangle([1, 4], [5, 6], [6, 1], [4, 5]) ➞ true
withinTriangle([1, 4], [5, 6], [6, 1], [3, 2]) ➞ false
withinTriangle([-6, 2], [-2, -2], [8, 4], [4, 2]) ➞ true

Notes
Pairs are given as arrays of length two.