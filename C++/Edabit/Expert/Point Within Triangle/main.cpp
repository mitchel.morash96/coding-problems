#include <iostream>
using namespace std;

bool withinTriangle(pair<int, int> p0, pair<int, int> p1, 
                    pair<int, int> p2, pair<int, int> test) {
    
    float mainTriArea = abs((p0.first*(p1.second-p2.second) + 
                          p1.first*(p2.second-p0.second) +
                          p2.first*(p0.second-p1.second)) /2);

    float A1 = abs((test.first*(p1.second-p2.second) + 
                 p1.first*(p2.second-test.second) +
                 p2.first*(test.second-p1.second)) /2);

    float A2 = abs((p0.first*(test.second-p2.second) + 
                 test.first*(p2.second-p0.second) +
                 p2.first*(p0.second-test.second)) /2);

    float A3 = abs((p0.first*(p1.second-test.second) + 
                 p1.first*(test.second-p0.second) +
                 test.first*(p0.second-p1.second)) /2);
    
    if (mainTriArea == A1 + A2 + A3) {
        return true;
    } else {
        return false;
    }
}

int main() {

    cout << withinTriangle({1, 4}, {5, 6}, {6, 1}, {4, 5}) << endl;
    cout << withinTriangle({1, 4}, {5, 6}, {6, 1}, {3, 2}) << endl;
    cout << withinTriangle({-6, 2}, {-2, -2}, {8, 4}, {4, 2}) << endl;

    return 0;
}