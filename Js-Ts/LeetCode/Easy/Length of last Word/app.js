/**
 * @param {string} s
 * @return {number}
 */
var lengthOfLastWord = function(s) {
    if (s.length === 0) return 0;
    
    var words = s.split(' ');
    for (var i = words.length; i > 0; i--) {
        if (words[i-1] !== '') {
            console.log(words[i-1]);
            return words[i-1].length;
        }
    }
};

// Found best solution
// var lengthOfLastWord = (s) => {
//     let arr = s.trim().split(' ');
//     return arr[arr.length-1].split('').length;
// }

let test1 = 'Hello World';
let test2 = '   fly me   to   the moon  ';
let test3 = 'luffy is still joyboy';

lengthOfLastWord(test1);
lengthOfLastWord(test2);
lengthOfLastWord(test3);