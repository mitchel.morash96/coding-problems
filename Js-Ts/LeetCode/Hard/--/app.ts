function ladderLength(beginWord: string, endWord: string, wordList: string[]): number {
    if (beginWord === endWord) return 0;
    if (!wordList.includes(endWord)) return 0;

    let currentWord: string = beginWord;
    let jumps: number = 0;
    let matchingLettersNeeded: number = beginWord.length-1;

    while (jumps <= wordList.length) {
        // check for word in list with single letter difference
        for (let i: number = 0; i < wordList.length; i++) {
            let matchingLetters: number = 0;
            for (let j: number = 0; j < wordList[i].length; j++) {
                if (currentWord[j] === wordList[i][j]) {
                    matchingLetters++;
                }
            }

            if (matchingLetters === matchingLettersNeeded) {
                break;
            }
        }
    }

    return jumps;
};


// 5
ladderLength('hit', 'cog', ['hot','dot','dog','lot','log','cog']);

// 0
ladderLength('hit', 'cog', ['hot','dot','dog','lot','log']);